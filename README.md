## Tecnologias Utilizadas

-- Backend --
1. Java: jdk-8u291
2. Springboot 4-4.10.0 (IDE)
---

## Instalacion

1. Descargar el proyecto 3it
2. Importar como proyecto Maven al IDE, en mi caso spring-tool-suite-4-4.10.0
3. Realizar update project por las dependencias en caso de no realizar descarga automatica.
4. Compilar el proyecto con maven clean install
5. Levantar App con el mismo server de springboot


## BD

1. Se agrega ademas un .sql
2. Importar la base de datos 
3. Verificar conexion a BD


## Version
0.0.1