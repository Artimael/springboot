package com.example.Test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600)
@RestController
@RequestMapping({ "/usuarios" })
public class Controlador {

	@Autowired
	UsuarioService service;

	@GetMapping
	public List<Usuario> listar() {
		return service.listar();
	}

	@PostMapping
	public Usuario agregar(@RequestBody Usuario u) {
		return service.add(u);
	}

//	@GetMapping(path = "/all")
//	public @ResponseBody Iterable<Usuario> getAllUsers() {
//		// This returns a JSON or XML with the users
//		return userRepository.findAll();
//	}

//	@GetMapping(path = "/all")
//	public @ResponseBody Iterable<Usuario> getAllUsers2() {
//		// This returns a JSON or XML with the users
//		return userRepository.findAll();
//	}

}