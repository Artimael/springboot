package com.example.Test;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepositorio userRepository;

	@Override
	public List<Usuario> listar() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
		// return null;
	}

	@Override
	public Usuario listarId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuario add(Usuario u) {
		return userRepository.save(u);
	}

}
