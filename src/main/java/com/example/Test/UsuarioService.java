package com.example.Test;

import java.util.List;

public interface UsuarioService {
	List<Usuario> listar();

	Usuario listarId(int id);

	Usuario add(Usuario u);
}
