package com.example.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TestApplicationTests {

	@Autowired
	private UsuarioService usuarios;

	@Test //Test que valida que se llenen los datos de la tabla de usuarios y se registren en el test, este valor dependera del numero de registros.
	public void testCantidadRegistrosTablaUsuario() {

		Integer usuariosBD = usuarios.listar().size();
		assertEquals(7, usuariosBD);
	}

	@Test
	void contextLoads() {
	}

}
